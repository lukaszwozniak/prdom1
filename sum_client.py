# -*- coding: utf-8 -*-
import logging
import logging.config
import socket
import sys


def client(aaa, bbb, logger):
    # Klient wysyla dwie liczby i dane do loggera automatycznie sa kontrolowane

    if not (isinstance(aaa, float) and isinstance(bbb, float)):
        try:
            aaa = float(aaa)
            bbb = float(bbb)
        except ValueError:
            return 0

    server_address = ('194.29.175.240', 6000)
    # Tworzenie gniazda TCP/IP

    p = socket.socket()
    # Połączenie z gniazdem nasłuchującego serwera

    logger.info(u'Laczenie z serwerem . . .')
    p.connect(server_address)

    try:
        logger.info(u'Liczby to: {0} i {1}...'.format(aaa,bbb))
        # Wysłanie danych
        p.sendall('{0} {1}'.format(aaa,bbb))

        # Odebranie odpowiedzi
        wynik = p.recv(2048)

        logger.info(u'Rezultat to: {0} + {1} = {2}'.format(aaa,bbb, wynik))

    except socket.error as exEr:
        # Obsługa socket.error
        return exEr[1]
    finally:
        # Zamknięcie połączenia na zakończenie działania
        p.close()


if __name__ == '__main__':
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger('sum_client')
    liczba1 = raw_input('Podaj pierwsza liczbe: ')
    liczba2 = raw_input('Podaj druga liczbe: ')



    client(liczba1, liczba2, logger)
