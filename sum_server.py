# -*- coding: utf-8 -*-

import socket
import logging
import logging.config

def server(logger):

    server_address = ('0.0.0.0', 6000)
    # Tworzenie gniazda TCP/IP


    p = socket.socket()
    p.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    # Powiązanie gniazda z adresem

    p.bind(server_address)

    # Nasłuchiwanie przychodzących połączeń
    p.listen(1)

    try:
        while True:
            # Nieskończona pętla pozwalająca obsługiwać dowolną liczbę połączeń
            # ale tylko jedno na raz

            # Czekanie na połączenie
            connectionone, addr = p.accept()


            try:
                # Odebranie danych
                data = str.split(connectionone.recv(2048))
                # Wyodrębnienie wyrażenia
                suma = float(data[0]) + float(data[1])

                # Wykonanie działania sumowania i odesłanie rezultatu
                logger.info(u'Sumuje liczby {0} i {1} oraz odsylam wynik = {2}...'.format(data[0], data[1], suma))
                connectionone.send('{0}'.format(suma))

            finally:
                # Zamknięcie połączenia
                connectionone.close()
    except KeyboardInterrupt:
        #       zakończenia działania serwera
        pass

if __name__ == '__main__':

    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger('sum_server')

    server(logger)