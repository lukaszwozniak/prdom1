# -*- coding: utf-8 -*-

import socket
import unittest

from sum_client import client

class Logger:

    def __init__(self):
        self.data = []

    def info(self, komunikat):
        self.data.append(komunikat)

class TestSumowania(unittest.TestCase):

    # Odebrana wiadomosc, pozniejsze wyluskanie danych
    odebraneDane = u'Liczby to: {0} i {1}...Rezultat to: {0} + {1} = {2}'

    def setUp(self):
        self.logger = Logger()

    def test1_SumaDodatnichLiczba(self):
        self.liczSume(1, 2)

    def test2_SumaUjemnychLiczb(self):
        self.liczSume(-12, -23)

    def test3_SumaStringLiczba(self):
        self.liczSume('alamakota', 10)

    def test4_SumaStringow(self):
        self.liczSume('alamakota', 'alaniemakota')


    def liczSume(self, aaa, bbb):
        self.wykonajSumowanie(aaa, bbb)
        try:
            aaa = float(aaa)
            bbb = float(bbb)
            # format danych
            odebranaInformacja = self.odebraneDane.format(aaa, bbb, aaa + bbb)
        except:
            odebranaInformacja = 'Nie wykryto przesylanych danych'

        odebraneWartosci = self.process_log()

        self.assertEqual(odebranaInformacja, odebraneWartosci)

    def process_log(self):
        if not self.logger.data:
            return 'Nie wykryto przesylanych danych'
        return self.logger.data[1] + self.logger.data[2]

    def wykonajSumowanie(self, aaa, bbb):
        try:
            client(aaa, bbb, self.logger)
        except socket.error as ExceptionError:
            if ExceptionError.errno == 61:
                msg = u'Blad: {0}, czy serwer dziala?'
                self.fail(msg.format(ExceptionError.strerror))
            else:
                self.fail(u'Nieznany blad: {0}'.format(str(ExceptionError)))


if __name__ == '__main__':
    unittest.main()